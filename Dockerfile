ARG PLAYWRIGHT_VERSION=1.25.2

FROM mcr.microsoft.com/playwright/python:v${PLAYWRIGHT_VERSION}-focal
ARG PLAYWRIGHT_VERSION

WORKDIR /workdir

ADD requirements.txt /workdir/

RUN pip install -r requirements.txt \
  && playwright install --with-deps

COPY ./scripts /workdir/

ENTRYPOINT ["python"]
CMD ["scripts/crawl.py"]

