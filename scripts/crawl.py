import re
from urllib.parse import urlparse, urljoin
import networkx as nx
from playwright.sync_api import sync_playwright, Page


def crawl(start_url: str, page: Page, folder: str, pages: int):
    visited = set()
    tovisit = set()
    tovisit.add(start_url)
    start = urlparse(start_url)
    graph = nx.DiGraph()
    while len(tovisit) > 0 and len(visited) <= pages:
        url = tovisit.pop()
        print(f'Visiting {url}')
        page.goto(url)
        visited.add(url)
        graph.add_node(url)
        u = urlparse(url)
        screenshot_name = re.sub('[^a-zA-Z0-9-_\\.]', '_', u.path)
        screenshot_filename = f'{folder}/{screenshot_name}.png'
        print(f'Screenshotting {u.path} to {screenshot_filename}')
        page.screenshot(
            path=screenshot_filename,
            animations='disabled',
            caret='hide',
            full_page=True,
            scale='device'
        )
        # Crawl for more
        for element in page.locator('a').element_handles():
            _href = element.get_attribute('href')
            if _href is None:
                continue
            _href = re.sub('#.*$', '', _href).strip()
            if len(_href) == 0 or (_href.startswith('http') and not _href.startswith(start.geturl())):
                continue
            _url = urljoin(start.geturl(), _href)
            if _url in visited or _url in tovisit or not _url.startswith('http') or _url.endswith('.pdf'):
                continue
            print(f'Found {_href}')
            tovisit.add(_url)
            graph.add_node(_url)
            graph.add_edge(url, _url)

if __name__ == '__main__':
    import os
    import sys

    filename = sys.argv[1]
    name, _ = os.path.splitext(os.path.basename(filename))
    folder = os.path.dirname(filename)
    urls = []
    with open(filename) as f:
        urls = [line for line in f.readlines() if len(line.strip()) > 0]

    with sync_playwright() as p:
        browser = p.chromium.launch()
        page = browser.new_page()

        for url in urls:
            result = urlparse(url)
            hostname = result.hostname
            folder = f'{folder}/{name}/{hostname}'
            print(f'Crawling {folder}')
            os.makedirs(folder, exist_ok=True)
            crawl(url, page=page, folder=folder, pages=100)

        browser.close()

