# Competitor Analysis

## List competitors

For each analysis you want to make, list all your competitors websites in `analysis/<name>.txt`.
Each line in the file is one website that will be followed.

## Run

```
make analysis/name.txt
```

