from typing import Any
import pytest
from playwright.sync_api import Playwright


@pytest.fixture(scope="session")
def base_url(pytestconfig: Any):
    yield pytestconfig.getoption('--base-url')

@pytest.fixture(scope="session")
def api_request_context(pytestconfig: Any, playwright: Playwright):
    base_url = pytestconfig.getoption("--base-url")
    request_context = playwright.request.new_context(base_url=base_url)
    yield request_context
    request_context.dispose()

