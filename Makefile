IMAGE_TAG = "registry.gitlab.com/parlant-co/competitor-analysis:main"

build:
	docker build -t $(IMAGE_TAG) .

analysis/%: build
	docker run -v$$(pwd)/analysis:/workdir/analysis -v$$(pwd)/scripts:/workdir/scripts -ti $(IMAGE_TAG) scripts/crawl.py $@

